package main

import "fmt"

func push(arr *[]int, element *int) {
	*arr = append(*arr, *element)
}

func pop(arr *[]int) {
	*arr = (*arr)[:len(*arr) - 1]
}

func top(arr *[]int) (element int) {
	element = (*arr)[len(*arr)-1]
	return element
}

func getMax(arr *[]int) (maxElement int) {
	maxElement = 0
	for i := 0; i < len(*arr); i++ {
		if (*arr)[i] > maxElement {
			maxElement = (*arr)[i]
		}
	}
	return maxElement
}


func main() {
	myArr := []int{10, 20, 80, 40, 50}
	myElement := 70

	push(&myArr, &myElement)
	fmt.Println(myArr)
	pop(&myArr)
	fmt.Println(myArr)
	lastElement := top(&myArr)
	fmt.Println(lastElement)
	maxElement := getMax(&myArr)
	fmt.Println(maxElement)
}
